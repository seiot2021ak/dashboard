package client;

import org.apache.commons.lang3.tuple.Pair;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface DamClient {


    String ALERT_URI = "alert";
    String PERCENTAGE_URI = "percentage";
    String MANUAL_URI = "manual";
    String WATER_LEVEL_URI = "waterlevel";

    /**
     * Check if client is ready
     * @return true if ready
     */
    boolean isReady();

    /**
     * Makes a request for the alert level of the dam
     * @return String representing a state of the dam
     */
    Optional<String> getAlert();

    /**
     * Makes a request for the opening percentage of the dam
     * @return Integer in the range 0-100 representing
     */
    Optional<Integer> getPercentage();

    /**
     * Makes a request for a list o the last entries of water level
     * @param quantity Unsigned Integer rapresenting the quantity
     * @return List of length less or equal quantity in Pairs of Timestamp and Integer
     */
    Optional<List<Pair<Timestamp, Integer>>> getWaterLevel(int quantity);

    /**
     * Makes a request for the last entry of water level
     * @return Pair of Timestamp and Integer
     */
    Optional<Pair<Timestamp, Integer>> getWaterLevel();

    /**
     * Makes a request for manual sate
     * @return Boolean
     */
    Optional<Boolean> getManual();
}
