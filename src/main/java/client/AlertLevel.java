package client;

public enum AlertLevel {
    NORMAL,
    PRE_ALARM,
    ALARM;

    public static AlertLevel getValue(String level) throws IllegalArgumentException{
        return valueOf(level.toUpperCase().replaceAll("[-_]","_"));
    }
}
