package client;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Timestamp;
import java.util.*;

public class HttpDamClient implements DamClient {
    public static final String HOST = "localhost:8080";
    private final String host;
    private final HttpClient client;
    private final Gson json;

    public HttpDamClient(String address) {
        this.host = address;
        client = HttpClient.newBuilder()
                .build();
        json = new GsonBuilder().setDateFormat("MMM dd, yyyy HH:mm:ss").create();
    }

    public HttpDamClient(String host, int port) {
        this(String.format("%s:%d",host,port));
    }

    public String getHost() {
        return host;
    }

    private Optional<HttpResponse<String>> handleRequest(String resource) {
        Optional<HttpResponse<String>> result = Optional.empty();
        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(String.format("http://%s/dam/%s", host, resource)))
                    .GET()
                    .build();
            result = Optional.ofNullable(client.send(request, HttpResponse.BodyHandlers.ofString()));
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    private <T> Optional<T> decodeBody(HttpResponse<String> body, Class<T> T) {
        return decodeBody(body, (Type) T);
    }

    private <T> Optional<T> decodeBody(HttpResponse<String> body, Type Type) {
        try {
            if (body.body().equals("")) {
                return Optional.empty();
            }
            return Optional.ofNullable(json.fromJson(body.body(), Type));
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private <T> Optional<T> generalRequest(String resource, Class<T> T) {
        Optional<HttpResponse<String>> responseOptional = handleRequest(resource);
        if (responseOptional.isPresent()) {
            HttpResponse<String> response = responseOptional.get();
            if (response.statusCode() == 200) {
                return decodeBody(response, T);
            }
        }
        return Optional.empty();
    }

    private <T> Optional<T> generalRequest(Type T) {
        Optional<HttpResponse<String>> responseOptional = handleRequest(DamClient.WATER_LEVEL_URI);
        if (responseOptional.isPresent()) {
            HttpResponse<String> response = responseOptional.get();
            if (response.statusCode() == 200) {
                return decodeBody(response, T);
            }
        }
        return Optional.empty();
    }

    @Override
    public boolean isReady() {
        return getAlert().isPresent();
    }

    @Override
    public Optional<String> getAlert() {
        Optional<String> alert = generalRequest(ALERT_URI, String.class);
        if (alert.isPresent()) {
            alert = Optional.of(alert.get().toUpperCase());
        }
        return alert;
    }

    @Override
    public Optional<Integer> getPercentage() {
        return generalRequest(PERCENTAGE_URI, Integer.class);
    }

    @Override
    public Optional<List<Pair<Timestamp, Integer>>> getWaterLevel(int quantity) {

        if ( quantity<0){
            return Optional.empty();
        }

        Optional<HttpResponse<? extends String>> responseOptional = Optional.empty();

        try {
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI(String.format("http://%s/dam/%s?quantity=%d", host, WATER_LEVEL_URI, quantity)))
                    .GET()
                    .build();
            responseOptional = Optional.of(client.send(request, HttpResponse.BodyHandlers.ofString()));
        } catch (URISyntaxException | IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if (responseOptional.isPresent()) {
            HttpResponse<? extends String> response = responseOptional.get();
            if (response.statusCode() == 200) {
                try {
                    return Optional.of(json.fromJson(response.body(),
                            new TypeToken<LinkedList<ImmutablePair<Timestamp, Integer>>>() {
                            }.getType()));
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<Pair<Timestamp, Integer>> getWaterLevel() {
        return generalRequest(new TypeToken<ImmutablePair<Timestamp, Integer>>() {
        }.getType());
    }

    @Override
    public Optional<Boolean> getManual() {
        return generalRequest(MANUAL_URI, Boolean.class);
    }
}
