package common;

public class Counter implements Comparable<Counter> {

    private final int maxCount;    // maximum value
    private final int resetValue;
    private int count;             // current value

    // create a new counter with the given parameters
    public Counter(int max) {
        this(0,max);
    }

    public Counter(int initialCount, int max) {
        maxCount = max;
        count = initialCount;
        this.resetValue=initialCount;
    }

    // increment the counter by 1
    public void increment() {
        if (count < maxCount) count++;
    }

    public boolean hasEnded(){
        return count >= maxCount;
    }

    public void reset(){count=this.resetValue;}

    // return the current count
    public int value() {
        return count;
    }

    // return a string representation of this counter
    public String toString() {
        return "Counter: " + count;
    }

    // compare two Counter objects based on their count
    public int compareTo(Counter that) {
        return this.count - that.count;
    }
}