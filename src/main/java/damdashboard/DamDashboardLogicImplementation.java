package damdashboard;

import client.AlertLevel;
import client.HttpDamClient;

import java.sql.Timestamp;
import java.util.Map;

public class DamDashboardLogicImplementation implements DashboardLogic{

    DataUpdater model = new DataUpdater(HttpDamClient.HOST,10);
    AlertLevel level;
    private boolean initialized;
    private Integer opening;
    private boolean manual;
    private Map<Timestamp, Integer> map;

    public DamDashboardLogicImplementation() {
        connect();
    }

    @Override
    public boolean isReady() {
        return this.initialized;
    }

    @Override
    public String getAlertLabel() {
        if(!isReady())throw new IllegalStateException();
        return this.level.name();
    }

    @Override
    public Integer getOpening() {
        if(!isReady())throw new IllegalStateException();
        return opening;
    }

    @Override
    public boolean getManualState() {
        if(!isReady())throw new IllegalStateException();
        return manual;
    }

    @Override
    public Map<Timestamp, Integer> getSeries() {
        if(!isReady())throw new IllegalStateException();
        return map;
    }

    @Override
    public boolean isChartVisible() {
        return !level.equals(AlertLevel.NORMAL)&& isReady();
    }

    @Override
    public boolean isOpeningVisible() {
        return !level.equals(AlertLevel.NORMAL) && isReady();
    }

    @Override
    public void update() {
        if(model.isReady()) {
            this.level = model.getAlert();
            this.map = model.getLastsWaterLevelList(10);
            this.manual = model.getManual();
            this.opening = model.getPercentage();
            this.initialized=true;
        }
    }

    private void connect(){
        model.start();
    }

    private void disconnect(){
        model.stop();
    }
}
