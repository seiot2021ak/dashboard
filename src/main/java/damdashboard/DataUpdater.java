package damdashboard;

import client.AlertLevel;
import client.DamClient;
import client.HttpDamClient;
import common.Counter;
import org.apache.commons.lang3.tuple.Pair;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

public class DataUpdater {

    private static final int MAX_QUANTITY = 20;

    private final int max_quantity;

    final DamClient client;
    private final Counter counter;

    boolean initialized = false;
    private AlertLevel alert;
    private Integer percentage;
    private Boolean manual;
    private final int quantity;
    private final Map<Timestamp, Integer> temp_map = new HashMap<>();


    final Thread thread = new Thread(this::updateCycle);
    private boolean alive;

    public boolean isReady() {
            return initialized;
    }

    public boolean isAlive() {
        synchronized (this) {
            return alive;
        }
    }

    //Data methods
    public AlertLevel getAlert() {
        return alert;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public Boolean getManual() {
        return manual;
    }

    public Map<Timestamp, Integer> getWaterLevelMap() {
        synchronized(temp_map) {
            return Map.copyOf(temp_map);
        }
    }

    public Map<Timestamp, Integer> getLastsWaterLevelList(int quantity) {
        quantity = Math.min(quantity,max_quantity);
        synchronized (temp_map) {
            return temp_map.entrySet().parallelStream()
                    .sorted(Map.Entry.comparingByKey())
                    .limit(quantity)
                    .collect(Collectors.toMap(Map.Entry::getKey,Map.Entry::getValue));
        }
    }

    public DataUpdater(String address, int quantity,int max_quantity){
        this.client=new HttpDamClient(address);
        this.quantity=quantity;
        this.max_quantity=Math.min(max_quantity,MAX_QUANTITY);
        counter=new Counter(10);
    }

    public DataUpdater(String address,int quantity){
        this(address, quantity, quantity);
    }

    private void updateCycle(){
        while(alive){
            if (client.isReady()){
                //make requests
                Optional<String> alert = client.getAlert();
                if(alert.isPresent()){
                    try {
                        this.alert = AlertLevel.getValue(alert.get());
                    }catch(IllegalArgumentException e){
                        //log error on message value
                    }
                }else{
                    counter.increment();
                }
                client.getPercentage().ifPresentOrElse(x->percentage=x, counter::increment);
                client.getManual().ifPresentOrElse(x->manual=x, counter::increment);

                Optional<List<Pair<Timestamp, Integer>>> water_list = client.getWaterLevel(quantity);
                if (water_list.isPresent()){
                    synchronized(temp_map){
                        water_list.get().forEach(p -> temp_map.putIfAbsent(p.getKey(),p.getValue()));
                        temp_map.entrySet().parallelStream()
                                .sorted((o1, o2) -> -o1.getKey().compareTo(o2.getKey()))
                                .skip(max_quantity)
                                .forEach(e->temp_map.remove(e.getKey()));
                    }
                }

                if(counter.value()==0){
                    initialized=true;
                }
                //TODO log counter value
                counter.reset();

            }
            try {
                //noinspection BusyWait
                Thread.sleep(300);
            } catch (InterruptedException e) {
                //Thread has been Interrupted
                alive=false;
                e.printStackTrace();
            }
        }
    }

    public void start(){
        if(!isAlive()){
            alive=true;
            thread.start();
        }
    }

    public void stop(){
        if(isAlive()){
            alive=false;
        }
    }
}
