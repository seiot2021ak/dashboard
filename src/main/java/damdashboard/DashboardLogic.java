package damdashboard;

import java.sql.Timestamp;
import java.util.Map;

public interface DashboardLogic {

    boolean isReady();
    String getAlertLabel() throws IllegalStateException;

    Integer getOpening() throws IllegalStateException;

    boolean getManualState() throws IllegalStateException;

    Map<Timestamp, Integer> getSeries();

    boolean isChartVisible();

    boolean isOpeningVisible();

    void update();
}
