package ui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.general.Series;
import org.jfree.data.time.*;
import org.jfree.data.xy.*;
import org.junit.jupiter.api.condition.DisabledOnJre;

import javax.swing.*;
import java.awt.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Graph {

    private static final String SERIES_TAG = "water level";

    public JPanel getComponent() {
        return graph;
    }

    private ChartPanel graph;

    private final TimeSeriesCollection collection=new TimeSeriesCollection();

    private final TimeSeries series = new  TimeSeries(SERIES_TAG);

    public Graph(){
        createUIComponents();
    }
    public void createUIComponents(){
        collection.addSeries(series);
        series.setMaximumItemCount(10);
        graph = new ChartPanel(createChart(collection));
        graph.setPopupMenu(null);
        graph.setDomainZoomable(false);
        graph.setRangeZoomable(false);
        graph.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        graph.setBackground(Color.white);
    }

    public void setSeries(Map<Timestamp,Integer> series){
        series.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .forEach(o -> this.series.addOrUpdate(new Second(o.getKey()), o.getValue()));
    }

    private void clearOldPoints(){

    }

    private JFreeChart createChart(XYDataset dataset) {
        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Graphic of the level of water",
                "Time",
                "Level (meters)",
                dataset,
                true,
                true,
                false
        );
        XYPlot plot = chart.getXYPlot();

        setAxis(plot);

        var renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));

        plot.setRenderer(renderer);
        plot.setBackgroundPaint(Color.white);

        plot.setRangeGridlinesVisible(true);
        plot.setRangeGridlinePaint(Color.BLACK);

        plot.setDomainGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.BLACK);

        chart.getLegend().setFrame(BlockBorder.NONE);

        chart.setTitle(new TextTitle("Graphic of the level of water",
                        new Font("Serif", java.awt.Font.BOLD, 18)
                )
        );
        return chart;
    }

    private void setAxis(XYPlot plot) {
        ValueAxis yAxis = plot.getRangeAxis();
        DateAxis xAxis = ((DateAxis) plot.getDomainAxis());
        xAxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
        yAxis.setAutoRange(false);
        yAxis.setRange(0,450);
    }

    public void clearChart(){
        series.clear();
    }
}
