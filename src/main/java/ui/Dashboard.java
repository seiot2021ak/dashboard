package ui;

import damdashboard.DamDashboardLogicImplementation;
import damdashboard.DashboardLogic;

import javax.swing.*;
import java.awt.*;
import java.sql.Timestamp;
import java.util.Map;

public class Dashboard extends JPanel {

    GridBagLayout layout= new GridBagLayout();
    Indicator statusIndicator = new Indicator("Status:");
    Indicator openingIndicator = new Indicator("Dam opening:");
    Indicator controlIndicator = new Indicator("Manual control:");

    private static final String DEFAULT_STRING = " - ";
    Graph graph = new Graph();

    DashboardUIUpdater updater = new DashboardUIUpdater();

    private void setAlert(String alert){
        this.statusIndicator.setValue(alert.toUpperCase());
    }

    private void setOpening(Integer value){
        this.openingIndicator.setValue(value +"%");
    }

    private void setManual(Boolean value){
        this.openingIndicator.setValue(value.toString());
    }

    private void setSeries(Map<Timestamp, Integer> series){
        this.graph.setSeries(series);
    }

    public Dashboard() {
        super(new GridBagLayout());
        initComponents();
        updater.start();
    }

    void initComponents(){
        GridBagConstraints i1 = new GridBagConstraints();
        i1.anchor = GridBagConstraints.CENTER;
        i1.insets = new Insets(0,0,0,0);
        i1.ipadx = 0;
        i1.weightx = 0.5;
        i1.fill = GridBagConstraints.HORIZONTAL;
        i1.gridx = 0;
        i1.gridy = 0;
        this.add(statusIndicator,i1);
        GridBagConstraints i2 = new GridBagConstraints();
        i2.ipadx = 0;
        i2.weightx = 0.5;
        i2.fill = GridBagConstraints.HORIZONTAL;
        i2.gridx = 1;
        i2.gridy = 0;
        this.add(openingIndicator,i2);
        GridBagConstraints i3 = new GridBagConstraints();
        i3.ipadx = 0;
        i3.weightx = 0.5;
        i3.fill = GridBagConstraints.HORIZONTAL;
        i3.gridx = 2;
        i3.gridy = 0;
        this.add(controlIndicator,i3);
        GridBagConstraints graphConstraints = new GridBagConstraints();
        graphConstraints.fill = GridBagConstraints.BOTH;
        graphConstraints.weightx=0.0;
        graphConstraints.gridx = 0;
        graphConstraints.gridy = 1;
        graphConstraints.gridwidth = 3;
        graphConstraints.insets = new Insets(10,10,10,10);
        this.add(graph.getComponent(),graphConstraints);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Dashboard");
        frame.setContentPane(new Dashboard());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private class DashboardUIUpdater {
        Thread thread = new Thread(this::run);
        //ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
        private boolean alive = false;
        private final DashboardLogic logic = new DamDashboardLogicImplementation();
        void start(){
            if(!alive){
                //pool.schedule(()->run(),100, TimeUnit.MILLISECONDS);
                thread.start();
                alive = true;
            }
        }
        void stop(){
            if(alive){
                alive = false;
            }
        }

        void run(){
            while(alive){
                if(logic.isReady()) {
                    setAlert(logic.getAlertLabel());
                    setManual(logic.getManualState());
                    if (logic.isOpeningVisible()) {
                        setOpening(logic.getOpening());
                    } else {
                        openingIndicator.setValue(DEFAULT_STRING);
                    }
                    if (logic.isChartVisible()) {
                        setSeries(logic.getSeries());
                    } else {
                        graph.clearChart();
                    }
                }
                logic.update();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    alive=false;
                    //throw new RuntimeException(e);
                }
            }
        }
    }



}
