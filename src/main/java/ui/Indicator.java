package ui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class Indicator extends JPanel {

    JLabel label;
    JTextField value;

    public Indicator(LayoutManager layout, String label) {
        super(layout);
        initComponents();
        this.label.setText(label);
    }


    public Indicator(String label) {
        super();
        this.setLayout(new BoxLayout(this,BoxLayout.LINE_AXIS));
        initComponents();
        this.label.setText(label);
    }

    private void initComponents(){
        this.label = new JLabel();
        this.label.setBorder(new EmptyBorder(0,0,0,5));
        this.value = new JTextField();
        this.value.setEditable(false);
        this.value.setHorizontalAlignment(JTextField.CENTER);
        this.value.setText(" - ");
        this.add(this.label);
        this.add(this.value);
        this.setBorder(new EmptyBorder(5, 5, 5, 5));
    }

    public void setValue(final String value){
        this.value.setText(value);
    }

    public String getValue(){
        return this.label.getText();
    }
}
