package client;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.Timestamp;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Disabled //Remote test with server
class HttpDamClientTest {

    public static final String HOST = "localhost:8080";
    private static final int RECORD_QUANTITY = 5;
    private static HttpDamClient client;

    @BeforeAll
    static void setUp() {
        client = new HttpDamClient(HOST);
    }

    @Test
    void getHost() {
        assertEquals(HOST,client.getHost());
    }

    @Test
    void getAlert() {
        Optional<String> alert = client.getAlert();
        assertTrue(alert.isPresent());
        assertEquals("NORMAL", alert.get());
    }

    @Test
    void getPercentage() {
        Optional<Integer> percentage = client.getPercentage();
        assertTrue(percentage.isPresent());
    }

    @Test
    void getWaterLevel() throws IOException, URISyntaxException, InterruptedException {
        Optional<Pair<Timestamp, Integer>> waterLevel = client.getWaterLevel();
        assertTrue(waterLevel.isEmpty());
        var response = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build().send(HttpRequest.newBuilder()
                        .uri(new URI("http://" + HOST + "/dam/" + DamClient.WATER_LEVEL_URI))
                        .PUT(HttpRequest.BodyPublishers.ofString(String.valueOf(1)))
                        .build(), HttpResponse.BodyHandlers.ofString());
        assertEquals(200,response.statusCode());
        waterLevel = client.getWaterLevel();
        assertTrue(waterLevel.isPresent());
    }

    @Test
    void testGetWaterLevel() {
        var waterLevel = client.getWaterLevel(RECORD_QUANTITY);
        assertTrue(waterLevel.isPresent());
        assertTrue(waterLevel.get().size()<=RECORD_QUANTITY);
    }

    @Test
    void getManual() {
        Optional<Boolean> manual = client.getManual();
        assertTrue(manual.isPresent());
    }
}